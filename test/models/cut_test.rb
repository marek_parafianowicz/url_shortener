require 'test_helper'

class CutTest < ActiveSupport::TestCase

  def setup
    @cut = Cut.new(original_url: "https://testing.com", short_url: "test.com/1a2b3c")
  end

  test "Reponds to errors" do
    assert_respond_to(@cut, :save)
  end

  test "Should be valid" do
  	assert @cut.valid?
  end

  test "Cut with blank original url should not be valid" do
    @cut.original_url = "   "
    assert_not @cut.valid?
  end

  test "Cut with blank short url should not be valid" do
    @cut.short_url = "   "
    assert_not @cut.valid?
  end

  test "Cut without original url should not be valid" do
    cut = Cut.new(short_url: "test.com/1a2b3c")
    assert_not cut.valid?
  end

  test "Cut without short url should not be valid" do
    cut = Cut.new(short_url: "https://googles2.com")
    assert_not cut.valid?
  end

  test "Original_url should be max 400 characters long" do
  	original_url = "http://" + "a"*390 + ".com"
    @cut.original_url = original_url
  	assert_not @cut.valid?
  end

  test "Original_url should be unical" do
    @cut.save
    another_cut = Cut.new(original_url: @cut.original_url, short_url: "test.com/nsabjk")
    assert_not another_cut.save
  end

  test "Short_url should be unical" do
    @cut.save
    another_cut = Cut.new(original_url: "wiki.com", short_url: @cut.short_url)
    assert_not another_cut.save
  end
end
