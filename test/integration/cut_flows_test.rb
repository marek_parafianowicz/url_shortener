require 'test_helper'

class CutFlowsTest < ActionDispatch::IntegrationTest
  
  test "Should shorten the url" do
    get "/"
    assert_response :success

    assert_difference('Cut.count', +1) do
	    post "/cuts", params: { cut: { original_url: "wikipedia.org" } }, xhr: true
	  end

	  # random_short_url = \Ahttps:\/\/link-shrtnr.herokuapp.com\/[a-z\d]{8}\z
   #  assert_select "a[href=?]", random_short_url
    assert_select "td", "https://wikipedia.org"
    # Is failing
    
  end

  test "Should show 2 shorten urls" do
    get "/"
    assert_response :success

    assert_difference('Cut.count', +1) do
	    post "/cuts", params: { cut: { original_url: "wikipedia.org" } }, xhr: true
	  end

	  assert_difference('Cut.count', +1) do
	    post "/cuts", params: { cut: { original_url: "google.org" } }, xhr: true
	  end

	  # assert_select 'table' 
	  # do |elements|
   #  	elements.each do |element|
   #  		assert_select element, "td", 2
   #  	end
   #  end
   # assert_select "td", "https://wikipedia.org"
   # assert_select "td", "https://google.com"

   # 	assert_select "tr", 2

		assert_select "tr" do 
  		assert_select "td", 2
  	end
  end

  test "Create only one cut if original urls are simmilar" do
    get "/"
    assert_response :success

    assert_difference('Cut.count', +1) do
      post "/cuts", params: { cut: { original_url: "wikipedia.org" } }, xhr: true
    end

    last_cut = Cut.last
    assert_equal "http://wikipedia.org", last_cut.original_url

    expected_short_url = "http://link-shrtnr.herokuapp.com/#{last_cut.short_url}"
    assert_select "tr td:nth-child(2)", expected_short_url
    # Is failing

    assert_no_difference('Cut.count') do
      post "/cuts", params: { cut: { original_url: "wikipedia.org" } }, xhr: true
    end



  end
end
