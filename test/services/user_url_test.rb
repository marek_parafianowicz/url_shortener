require 'test_helper'

class UserUrlTest < ActiveSupport::TestCase

	def setup
		@prepared_url = "http://onet.pl"
	end

	test "Reponds to new" do
		assert UserUrl.new("long_url")
	end

	test "Prepare url with www" do
		user_url = UserUrl.new("www.onet.pl")
		assert_equal @prepared_url, user_url.prepare_url
		# Is failing
	end

	test "Prepare url with http" do
		user_url = UserUrl.new("http://onet.pl")
		assert_equal @prepared_url, user_url.prepare_url
	end

	test "Prepare url with https" do
		user_url = UserUrl.new("https://onet.pl")
		assert_equal @prepared_url, user_url.prepare_url
		# Is failing
	end

	test "Prepare url without schema" do
		user_url = UserUrl.new("onet.pl")
		assert_equal @prepared_url, user_url.prepare_url
	end

	test "Prepare CAPS url" do
		user_url = UserUrl.new("ONET.PL")
		assert_equal @prepared_url, user_url.prepare_url
	end
end