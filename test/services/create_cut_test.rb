require 'test_helper'

class CreateCutTest < ActiveSupport::TestCase

	test "Reponds to prepare" do
		assert_respond_to(CreateCut.new(original_url: "long_url.com"), :prepare)
	end

	test "Creates valid cut" do
		create_cut = CreateCut.new(original_url: "ascac")
		assert create_cut.prepare
	end

	test "Creates new cut" do
		create_cut = CreateCut.new(original_url: "ascac")
		assert_difference('Cut.count', +1) do
	    create_cut.prepare
	  end	
	end

	test "Doesn't create cut when it's simillar to already added" do
		create_cut = CreateCut.new(original_url: "ascac")
		create_cut.prepare
	  second_create_cut = CreateCut.new(original_url: "ASCAC")
	  assert_no_difference 'Cut.count' do
	    second_create_cut.prepare
	  end
	end	

	test "Doesn't create cut if original url is blank" do
		create_cut = CreateCut.new(original_url: "   ")
	  assert_no_difference 'Cut.count' do
			create_cut.prepare
	  end
	end	

	

	

end