class UserUrl
	def initialize(url)
		@url = url.downcase
	end

	def prepare_url
    return @url if @url.blank?
   

    uri = URI.parse(@url)
    unless uri.kind_of?(URI::HTTP)
      @url.insert(0, "http://")
    end
 	  @url
	end
end