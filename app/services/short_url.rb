class ShortUrl

	def self.random_sufix
		[*('a'..'z'),*('0'..'9')].shuffle[0,8].join
	end
end