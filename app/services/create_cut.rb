class CreateCut
  attr_reader :prepare
  # def initialize(:url)
  def initialize(params, random_sufix_manager = ShortUrl)
    @url = params.fetch :original_url
    @random_sufix_manager = random_sufix_manager
  end

  def prepare
   first_or_create
  end

private
  def first_or_create  
    Cut
      .where(original_url: long_url)
      .first_or_create(original_url: long_url, short_url: @random_sufix_manager.random_sufix)
  end

  def long_url
    @long_url ||= UserUrl.new(@url).prepare_url
  end

end