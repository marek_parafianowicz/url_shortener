class Cut < ApplicationRecord
	
	validates :original_url, presence: true, length: {maximum: 400},
													 uniqueness: { case_sensitive: false } 
	validates :short_url, presence: true, uniqueness: { case_sensitive: false }

	# before_validation :url_scheme 

	# protected
	# 	def url_scheme
	# 		if self.original_url.blank?
	# 			self.original_url
	# 		else
	# 			uri = URI.parse(self.original_url)
	# 			unless uri.kind_of?(URI::HTTP)
	# 				self.original_url.insert(0, "https://")
	# 			end
	# 		end
	# 	end
end
