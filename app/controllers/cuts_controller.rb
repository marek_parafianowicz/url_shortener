class CutsController < ApplicationController
  respond_to :js

  def index
    @cut = Cut.new
  end

  def create
    @cut = CreateCut.new(cut_params).prepare
    #binding.pry
    respond_with @cut
  end

  def show
    cut = Cut.find_by(short_url: params[:short_url])
    redirect_to cut.original_url
  end

  private
  
    def cut_params
      params.require(:cut).permit(:original_url, :short_url)
    end      
end
