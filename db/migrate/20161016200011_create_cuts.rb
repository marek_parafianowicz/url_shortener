class CreateCuts < ActiveRecord::Migration[5.0]
  def change
    create_table :cuts do |t|
      t.string :original_url
      t.string :short_url

      t.timestamps
    end
    add_index :cuts, :original_url
  end
end
