class ChangeOriginalUrlToText < ActiveRecord::Migration[5.0]
  def change
  	change_column :cuts, :original_url, :text
  end
end
