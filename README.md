# Url shortener

[Url shortener](https://link-shrtnr.herokuapp.com/) shorten links to make them easier to share.

## Prerequisites

Shortener is written in Rails 5.0.0.1 which requires Ruby language 2.2.2 or newer.

For Ruby installation instruction visit [official ruby language website](https://www.ruby-lang.org/en/).
For Rails installation instruction visit [rails guides](http://guides.rubyonrails.org/getting_started.html).


## Installation

```bash
$ git clone git@bitbucket.org:marek_parafianowicz/url_shortener.git
$ cd url_shortener
$ rake db:migrate
```

## Running/Development

* `$ rails s`
* Visit your app at [http://localhost:3000](http://localhost:3000).

## Running tests

* `$ rails test`

## Deploying

Url shortener is now deployed to [heroku](https://link-shrtnr.herokuapp.com/).
To deploy it in similar way, follow the next steps:

* Heroku uses PostgrSQL so remember to add `pg` gem. You may restrict it to production environment:

```ruby
group :production do
  gem 'pg', '0.18.4'
end
```

`$ bundle install --without production`

* Commit version ready for production:

```bash
$ git add -A
$ git commit -m “Your message”
```

* Create an account on [Heroku](https://heroku.com)

`$ heroku login` using your credentials

* Deploy:

```bash
$ heroku create
$ git push heroku master
$ heroku open
```
