Rails.application.routes.draw do
  # get 'cuts/new'
  # get 'cuts/show'
  # get 'cuts/create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'cuts#index'
  get '/:short_url', to: 'cuts#show'#, as: 'cut'
  resources :cuts, only: [:new, :index, :create]
end
